cmake_minimum_required(VERSION 2.8.4)
project(trill)

find_package(Curses REQUIRED)

add_subdirectory(libtrello)
include_directories(libtrello)
find_library(TRELLO libtrello)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99")

set(SOURCE_FILES main.c)

add_executable(trill ${SOURCE_FILES})
target_link_libraries(trill ${CURSES_LIBRARIES} libtrello)
