#include <stdlib.h>
#include <libtrello.h>
#include <ncurses.h>

#define STATE_QUIT 0
#define STATE_BOARD_LIST 1
#define STATE_BOARD_DETAIL 2

trello_array boards_array;
trello_array current_lists;
trello_array current_cards;
int scr_width, scr_height;

int state_board_list(int*);
int state_board_detail(int);
static void cleanup();

void get_boards_callback(trello_array b_array) {
    boards_array = b_array;
    // TODO: cache boards in local file
}

void get_lists_callback(trello_array l_array) {
    current_lists = l_array;
}

void get_cards_callback(trello_array c_array) {
    current_cards = c_array;
}

int main(int argc, char *argv[]) {

    // initialize ncurses
    initscr();
    start_color();
    raw();
    noecho();
    nodelay(stdscr, true);
    keypad(stdscr, true);
    getmaxyx(stdscr, scr_height, scr_width);
    init_pair(1, COLOR_BLACK, COLOR_RED);
    init_pair(2, COLOR_WHITE, COLOR_BLUE);

    // initialize file variables
    boards_array.size = 0;
    boards_array.el.boards = NULL;
    current_lists.size = 0;
    current_lists.el.lists = NULL;
    current_cards.size = 0;
    current_cards.el.cards = NULL;

    // fetch initial trello data in background
    get_boards(get_boards_callback);

    // main program loop
    int next_state = STATE_BOARD_LIST;
    int selected_board;
    while (next_state != STATE_QUIT) {
        if (next_state == STATE_BOARD_LIST) {
            next_state = state_board_list(&selected_board);
        } else if (next_state == STATE_BOARD_DETAIL) {
            next_state = state_board_detail(selected_board);
        }
    }

    // shutdown
    cleanup();
    return 0;
}

void animate_load(const char *msg) {
    static char spinner = '|';
    mvprintw(scr_height - 1, 0, "%s %c", msg, spinner);
    refresh();
    if      (spinner == '\\') { spinner = '|';  }
    else if (spinner == '|')  { spinner = '/';  }
    else if (spinner == '/')  { spinner = '-';  }
    else if (spinner == '-')  { spinner = '\\'; }
}

int state_board_list(int *selected_board) {

    if (boards_array.size == 0) {
        get_boards(get_boards_callback);
        attrset(COLOR_PAIR(1));
        while (boards_array.size == 0) {
            animate_load("fetching list of boards...");
            if (getch() == 27) {
                return STATE_QUIT;
            }
        }
    }

    int ch, current_board = 0, refresh = 1;
    while (true) {
        // handle input
        ch = getch();
        if (ch == 27) { // escape key
            return STATE_QUIT;
        } else if (ch == KEY_UP && current_board > 0) {
            current_board--;
            refresh = 1;
        } else if (ch == KEY_DOWN && current_board < boards_array.size-1) {
            current_board++;
            refresh = 1;
        } else if (ch == 10) {
            *(selected_board) = current_board;
            return STATE_BOARD_DETAIL;
        } else if (ch != -1) {
            fprintf(stderr, "you pressed %d", ch);
        }
        // redraw screen
        if (refresh) {
            attrset(COLOR_PAIR(2));
            mvprintw(1, 1, "Board List");
            attrset(A_NORMAL);
            for (size_t i = 0; i < boards_array.size; i++) {
                if (i == current_board) {
                    attrset(A_REVERSE);
                }
                mvprintw((i*2) + 3, 3, boards_array.el.boards[i].name);
                if (i == current_board) {
                    attrset(A_NORMAL);
                }
            }
            refresh();
        }
    }
}

int state_board_detail(int selected_board) {
    get_lists(boards_array.el.boards[i].id, get_lists_callback);
    get_cards(boards_array.el.boards[i].id, get_cards_callback);

    while (current_lists.size == 0 || current_cards.size == 0) {
        attrset(COLOR_PAIR(1));
        animate_load("loading board details...");
        if (getch() == 27) {
            return STATE_QUIT;
        }
    }

    int ch, refresh = 1, current_card = 0, current_list = 0;
    while (true) {
        ch = getch();
        if (ch == 27) {
            return STATE_QUIT;
        } else if (ch == KEY_UP && current_card > 0) {
            current_card--;
            refresh = 1;
        } else if (ch == KEY_DOWN && current_card < current_cards.size-1) {
            current_card++;
            refresh = 1;
        } else if (ch == KEY_LEFT && current_list > 0) {
            current_list--;
            refresh = 1;
        } else if (ch == KEY_RIGHT && current_list < current_lists.size-1) {
            current_list++;
            refresh = 1;
        }

        if (refresh) {
            attrset(COLOR_PAIR(2));
            mvprintw(1, 1, boards_array.el.boards[selected_board].name);
        }
    }

    return STATE_BOARD_LIST;
}

static void cleanup() {
    endwin();
    trello_free_boards(&boards_array);
}
